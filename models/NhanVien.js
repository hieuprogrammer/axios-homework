function NhanVien() {
    this.maNhanVien = '';
    this.tenNhanVien = '';
    this.chucVu = '';
    this.heSoChucVu = '';
    this.luongCoBan = '';
    this.soGioLamTrongThang = '';

    this.xepLoaiNhanVien = function () {
        if (this.soGioLamTrongThang < 0) {
            return `Số giờ làm không thể là số âm!`;
        } else if (this.soGioLamTrongThang < 80) {
            return `⭐`;
        } else if (this.soGioLamTrongThang >= 80 && this.soGioLamTrongThang <= 160) {
            return `⭐⭐`;
        } else if (this.soGioLamTrongThang >= 160 && this.soGioLamTrongThang <= 240) {
            return `⭐⭐⭐`;
        } else if (this.soGioLamTrongThang >= 240 && this.soGioLamTrongThang <= 320) {
            return `⭐⭐⭐⭐`;
        } else if (this.soGioLamTrongThang >= 320 && this.soGioLamTrongThang <= 400) {
            return `⭐⭐⭐⭐⭐`;
        } else {
            return `Bạn được thăng chức! 😃`;
        }
    }
}