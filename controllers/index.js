(getDataFromApi = () => {
    try {
        var promise = axios({
            url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/LayDanhSachNhanVien`,
            method: `GET`,
            responseType: `json`
        });

        promise.then((response) => {
            console.log('Response: ', response.data);
            renderHtmlDataTable(response.data);
        });
    } catch (error) {
        promise.catch((error) => {
            console.log(`${error.response?.data}`);
            alert(`${error.response?.data}`);
        });
    }
})();

renderHtmlDataTable = (danhSachNhanVien) => {
    console.log('danhSachNhanVien: ', danhSachNhanVien);
    var innerHtmlContent = ``;

    for (var index = 0; index < danhSachNhanVien.length; index++) {
        var nhanVien = danhSachNhanVien[index];
        console.log('nhanVien:', nhanVien);
        innerHtmlContent += `
                <tr>
                    <td>${nhanVien.maNhanVien}</td>
                    <td>${nhanVien.tenNhanVien}</td>
                    <td>${nhanVien.chucVu}</td>
                    <td>${nhanVien.heSoChucVu}</td>
                    <td>${nhanVien.luongCoBan}</td>
                    <td>${nhanVien.soGioLamTrongThang}</td>
                    <td style="width: 12.5rem;">
                        <button class="btn btn-success" onclick="capNhatNhanVien('${nhanVien.maNhanVien}')">Chọn</button>
                        <button class="btn btn-danger" onclick="xoaNhanVien('${nhanVien.maNhanVien}')">Xóa</button>
                    </td>
                </tr>`;
    }

    document.querySelector(`#table-danh-sach-nhan-vien-data`).innerHTML = innerHtmlContent;
}

var validation = new Validation();
document.querySelector(`#btn-them-nhan-vien`).addEventListener(`click`, function() {
    var nhanVien = new NhanVien();
    nhanVien.maNhanVien = document.querySelector(`#ma-nhan-vien`).value;
    nhanVien.tenNhanVien = document.querySelector(`#ten-nhan-vien`).value;
    nhanVien.chucVu = document.querySelector(`#chuc-vu`).value;
    nhanVien.heSoChucVu = document.querySelector(`#he-so-chuc-vu`).value;
    nhanVien.luongCoBan = document.querySelector(`#luong-co-ban`).value;
    nhanVien.soGioLamTrongThang = document.querySelector(`#so-gio-lam-trong-thang`).value;

    //kiểm tra hợp lệ (validation)
    var valid = true;

    //kiểm tra độ dài
    valid &= validation.kiemTraDoDai(nhanVien.maNhanVien,'#error_minMaxLength_maNhanVien', 4, 6);

    //kiểm tra input phải là ký tự
    valid &= validation.kiemTraTatCaKyTu(nhanVien.tenNhanVien,'#error_allLetter_tenNhanVien');

    //kiểm tra giá trị
    valid &= validation.kiemTraGiaTri(nhanVien.luongCoBan,'#error_minMaxValue_luongCoBan', 1000000, 20000000) & validation.kiemTraGiaTri(nhanVien.soGioLamTrongThang, '#error_minMaxValue_soGioLamTrongThang', 50, 150)

    if(!valid) return;

    try {
        var promise = axios({
            url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/ThemNhanVien`,
            method: `POST`,
            data: nhanVien
        });

        promise.then((response) => {
            console.log('Response: ', response.data);
            getDataFromApi();
        });
    } catch (error) {
        console.log(`${error.response?.data}`);
        alert(`${error.response?.data}`);
    }
});

xoaNhanVien = (maNhanVien) => {
    try {
        var promise = axios({
            url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/XoaNhanVien?maSinhVien=${maNhanVien}`,
            method: `DELETE`
        });

        promise.then((response) => {
            console.log('Response: ', response.data);
            getDataFromApi();
        });
    } catch(error) {
        console.log(`${error.response?.data}`);
        alert(`${error.response?.data}`);
    }
}

clearInput = () => {
    document.querySelector(`#ma-nhan-vien`).value = ``;
    document.querySelector(`#ten-nhan-vien`).value = ``;
    document.querySelector(`#chuc-vu`).value = ``;
    document.querySelector(`#he-so-chuc-vu`).value = ``;
    document.querySelector(`#luong-co-ban`).value = ``;
    document.querySelector(`#so-gio-lam-trong-thang`).value = ``;
}

capNhatNhanVien = (maNhanVien) => {
    try {
        var promise = axios({
            url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/LayThongTinNhanVien?maNhanVien=${maNhanVien}`,
            method: `GET`
        });

        promise.then((response) => {
            console.log('Response: ', response.data);

            var nhanVien = response.data;
            document.querySelector(`#ma-nhan-vien`).value = nhanVien.maNhanVien;
            document.querySelector(`#ten-nhan-vien`).value = nhanVien.tenNhanVien;
            document.querySelector(`#chuc-vu`).value = nhanVien.chucVu;
            document.querySelector(`#he-so-chuc-vu`).value = nhanVien.heSoChucVu;
            document.querySelector(`#luong-co-ban`).value = nhanVien.luongCoBan;
            document.querySelector(`#so-gio-lam-trong-thang`).value = nhanVien.soGioLamTrongThang;
            document.querySelector(`#ma-nhan-vien`).disabled = true;
        });
    } catch (error) {
        console.log(`${error.response?.data}`);
        alert(`${error.response?.data}`);
    }

    clearInput();
}

document.querySelector(`#btn-cap-nhat-nhan-vien`).onclick = () => {
    var nhanVien = new NhanVien();
    nhanVien.maNhanVien = document.querySelector(`#ma-nhan-vien`).value;
    nhanVien.tenNhanVien = document.querySelector(`#ten-nhan-vien`).value;
    nhanVien.chucVu = document.querySelector(`#chuc-vu`).value;
    nhanVien.heSoChucVu = document.querySelector(`#he-so-chuc-vu`).value;
    nhanVien.luongCoBan = document.querySelector(`#luong-co-ban`).value;
    nhanVien.soGioLamTrongThang = document.querySelector(`#so-gio-lam-trong-thang`).value;

    try {
        var promise = axios({
            url: `http://svcy.myclass.vn/api/QuanLyNhanVienApi/CapNhatThongTinNhanVien?maNhanVien=${nhanVien.maNhanVien}`,
            method: `PUT`,
            data: nhanVien
        });

        promise.then((response) => {
            console.log('Response: ', response.data);
            getDataFromApi();
        });
    } catch(error) {
        console.log(`${error.response?.data}`);
        alert(`${error.response?.data}`);
    }
};